package com.example.chahinaz.belshifaproducts.adaptors;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.example.chahinaz.belshifaproducts.R;


import com.example.chahinaz.belshifaproducts.entities.Product;

import java.util.ArrayList;
import java.util.PropertyResourceBundle;

public class ProductAdaptor extends ArrayAdapter implements SectionIndexer {

    Context context;
    ArrayList<Product> productList;
    Activity activity;
    ArrayList<Product> sectionArray;

    public ProductAdaptor(Activity context, ArrayList<Product> products) {
        super(context, R.layout.single_product, products);
        this.context = context;
        this.productList = products;
        this.activity = context;
        //setSectionArray();
        UpdateSectionArrayTask service = new UpdateSectionArrayTask();
        service.execute();
    }



    @NonNull
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;

//        if(view == null){
            LayoutInflater influater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = influater.inflate(R.layout.single_product,null,true);
            LinearLayout header = (LinearLayout) view.findViewById(R.id.productParent);
            String pos = productList.get(position).getName();
            char firstChar = pos.toUpperCase().charAt(0);
            if(productList.get(position).getItemID() == 13126) {

                Log.e("product", productList.get(position).getName());
            }
            if (position == 0) {
                setSection(header, pos);
               // sectionArray.add(new Product(position, productList.get(position).getName(),productList.get(position).getPrice()));
            } else {
                String preLabel = productList.get(position - 1).getName();
                char preFirstChar = preLabel.toUpperCase().charAt(0);
                if (firstChar != preFirstChar) {
                    setSection(header, pos  );
                  //  sectionArray.add(new Product(position, productList.get(position).getName(),productList.get(position).getPrice()));
                } else {
                    header.setVisibility(View.GONE);
                }
            }

            TextView name = (TextView) view.findViewById(R.id.itemName);
            TextView price = (TextView)  view.findViewById(R.id.itemPrice);

            name.setText(productList.get(position).getName());
            price.setText(String.valueOf(productList.get(position).getPrice()) + " EGP");
//        }
        return view;
    }

    private void setSection(LinearLayout header, String label) {
        TextView text = new TextView(context);
        header.setBackgroundColor(0xffaabbcc);
        text.setTextColor(Color.WHITE);
        text.setText(label.substring(0, 1).toUpperCase());
        text.setTextSize(20);
        text.setPadding(5, 0, 0, 0);
        text.setGravity(Gravity.CENTER_VERTICAL);
        header.addView(text);
    }

    public int getCount() {
        return productList.size();
    }
    public Object getItem(int arg0) {
        return productList.get(arg0);
    }
    public long getItemId(int arg0) {
        return 0;
    }

    public int getPositionForSection(int section) {
        if (section == 35) {
            return 0;
        }
        for (int i = 0; i < sectionArray.size(); i++) {
            String l = sectionArray.get(i).getName();
            char firstChar = l.toUpperCase().charAt(0);
            if (firstChar == section) {
                return sectionArray.get(i).getItemID();
            }
        }
//        for (int i = 0; i < productList.size(); i++) {
//            String l = productList.get(i).getName();
//            char firstChar = l.toUpperCase().charAt(0);
//            if (firstChar == section) {
//                return i;
//            }
//        }
        return -1;
    }
    public int getSectionForPosition(int arg0) {
        return 0;
    }
    public Object[] getSections() {
        return null;
    }

    @Override
    public final boolean hasStableIds() {
        return true;
    }

    private  class UpdateSectionArrayTask extends AsyncTask {
        private int mPosition;
        private RecyclerView.ViewHolder mHolder;

        public UpdateSectionArrayTask() {
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            return setSectionArray();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }

        public ArrayList<Product> setSectionArray() {
            sectionArray = new ArrayList<>();
            for(int i = 0; i < productList.size(); i++) {
                if(i == 0){
                    sectionArray.add(new Product(i, productList.get(i).getName(), 0));
                } else {
                    char firstChar = productList.get(i).getName().toUpperCase().charAt(0);
                    char preFirstChar = productList.get(i-1).getName().toUpperCase().charAt(0);
                    if (firstChar != preFirstChar) {
                        sectionArray.add(new Product(i, productList.get(i).getName(), 0));
                    }
                }
            }
            return sectionArray;
        }

    }

}
