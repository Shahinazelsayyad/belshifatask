package com.example.chahinaz.belshifaproducts.basic;

public class Constant {


    public  static final String SERVER_URL = "http://belshifadev.blshifa.com";

    public static class APILinks
    {
        public static final String PRODUCTS_URL = SERVER_URL + "/en_US/api/v1/products/alphabetical";
        public static final String PRODUCTS_URL_ABS = "/en_US/api/v1/products/alphabetical";

    }

}
