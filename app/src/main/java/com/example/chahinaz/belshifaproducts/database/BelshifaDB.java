package com.example.chahinaz.belshifaproducts.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.example.chahinaz.belshifaproducts.entities.Product;

import java.util.ArrayList;

public class BelshifaDB extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1 ;
    public static final String DATABASE_NAME = "Belshifa.db";
    private static final String ENCODING_SETTING = "PRAGMA encoding ='windows-1256'";
    Context context;

    public BelshifaDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        if (!db.isReadOnly()) {
            db.execSQL(ENCODING_SETTING);
        }
    }

    public void onCreate(SQLiteDatabase db) {
        try{
            final String SQL_CREATE_ENTRIES_PRODUCTS =
                    "CREATE TABLE " + ProductsTables.ProductTable.TABLE_NAME + " (" +
                            ProductsTables.ProductTable.COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " +
                            ProductsTables.ProductTable.COLUMN_NAME_NAME + " TEXT, " +
                            ProductsTables.ProductTable.COLUMN_NAME_PRICE + " INTEGER)";

            db.execSQL(SQL_CREATE_ENTRIES_PRODUCTS);
        }catch (Exception ex){
            Log.e("create database error", ex.getMessage());
        }


    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        final String SQL_DELETE_ENTRIES_PRODUCTS =
                "DROP TABLE IF EXISTS " + ProductsTables.ProductTable.TABLE_NAME ;
        db.execSQL(SQL_DELETE_ENTRIES_PRODUCTS);

        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertProduct(Product product){
        long newRowId = -1;
        try{
            // Gets the data repository in write mode
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(ProductsTables.ProductTable.COLUMN_NAME_ID, product.getItemID());
            values.put(ProductsTables.ProductTable.COLUMN_NAME_NAME, product.getName());
            values.put(ProductsTables.ProductTable.COLUMN_NAME_PRICE, product.getPrice());

            // Insert the new row, returning the primary key value of the new row
            newRowId = db.insert(ProductsTables.ProductTable.TABLE_NAME, null, values);

        } catch (Exception ex){
            Log.e("insert product", ex.getMessage());
        }
        return newRowId;
    }

    public long insertProducts(ArrayList<Product> itemsArray){
        // Gets the data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();
        long newRowId = -1;
        String sql = "insert into " + ProductsTables.ProductTable.TABLE_NAME + " (" + ProductsTables.ProductTable.COLUMN_NAME_ID + ", " + ProductsTables.ProductTable.COLUMN_NAME_NAME+ ", " + ProductsTables.ProductTable.COLUMN_NAME_PRICE + ") values (?, ?, ?);";

      //  this.getWritableDatabase();
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement(sql);

        for (int i = 0; i < itemsArray.size(); i++) {
            //generate some values

            stmt.bindLong(1, itemsArray.get(i).getItemID());
            stmt.bindString(2, itemsArray.get(i).getName());
            stmt.bindDouble(3, itemsArray.get(i).getPrice());

            long entryID = stmt.executeInsert();
            stmt.clearBindings();
        }

        db.setTransactionSuccessful();
        db.endTransaction();

        this.close();
        return newRowId;
    }

    public ArrayList<Product> selectProducts(){

        ArrayList<Product> products = null;
        try
        {

            // Gets the data repository in write mode
            SQLiteDatabase db = this.getReadableDatabase();

            // Define a projection that specifies which columns from the database
            // you will actually use after this query.
            String[] projection = {
                    ProductsTables.ProductTable.COLUMN_NAME_ID,
                    ProductsTables.ProductTable.COLUMN_NAME_NAME,
                    ProductsTables.ProductTable.COLUMN_NAME_PRICE,
            };

            // How you want the results sorted in the resulting Cursor
            String sortOrder =
                    ProductsTables.ProductTable.COLUMN_NAME_NAME + " COLLATE NOCASE ASC";

            Cursor cursor = null;
            products = new ArrayList<>();

            cursor = db.query(
                    ProductsTables.ProductTable.TABLE_NAME,                     // The table to query
                    projection,                               // The columns to return
                    null,                                // The columns for the WHERE clause
                    null,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder,                                // The sort order
                    null                );

            try{
                while(cursor.moveToNext()) {
                    int itemid = cursor.getInt(cursor.getColumnIndex(ProductsTables.ProductTable.COLUMN_NAME_ID));
                    String name = cursor.getString(cursor.getColumnIndexOrThrow(ProductsTables.ProductTable.COLUMN_NAME_NAME));
                    Double price = cursor.getDouble(cursor.getColumnIndexOrThrow(ProductsTables.ProductTable.COLUMN_NAME_PRICE));

                    Product product = new Product(itemid, name, price);
                    products.add(product);
                }

                cursor.close();
            }catch (Exception ex){
                ex.getMessage();
            }

            return products;
        } catch (Exception ex){
            Log.e("select products", ex.getMessage());
        }

        return products;
    }



    //call this in activity destroy
    public void closedDBConnection() {
        this.close();
    }
}
