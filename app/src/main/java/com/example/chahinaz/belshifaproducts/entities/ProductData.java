package com.example.chahinaz.belshifaproducts.entities;

import java.util.ArrayList;

public class ProductData {

    private String status;
    private ArrayList<Product> data;
    private String error;

    public ProductData(String status, ArrayList<Product> data, String error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Product> getData() {
        return data;
    }

    public void setData(ArrayList<Product> data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
