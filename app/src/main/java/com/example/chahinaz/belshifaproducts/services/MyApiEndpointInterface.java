package com.example.chahinaz.belshifaproducts.services;

import com.example.chahinaz.belshifaproducts.entities.Product;
import com.example.chahinaz.belshifaproducts.entities.ProductData;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.example.chahinaz.belshifaproducts.basic.Constant.APILinks.PRODUCTS_URL;
import static com.example.chahinaz.belshifaproducts.basic.Constant.APILinks.PRODUCTS_URL_ABS;
import static com.example.chahinaz.belshifaproducts.basic.Constant.SERVER_URL;

public interface MyApiEndpointInterface {

    // Request method and URL specified in the annotation

    public static final String BASE_URL = SERVER_URL;

    @GET(PRODUCTS_URL_ABS)
    Call<ProductData> getProducts(@Query("page") Integer page,
                                  @Query("limit") Integer limit);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
