package com.example.chahinaz.belshifaproducts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chahinaz.belshifaproducts.adaptors.ProductAdaptor;
import com.example.chahinaz.belshifaproducts.adaptors.SideBar;
import com.example.chahinaz.belshifaproducts.basic.Constant;
import com.example.chahinaz.belshifaproducts.database.BelshifaDB;
import com.example.chahinaz.belshifaproducts.entities.Product;
import com.example.chahinaz.belshifaproducts.entities.ProductData;
import com.example.chahinaz.belshifaproducts.services.MyApiEndpointInterface;

import com.google.gson.Gson;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.chahinaz.belshifaproducts.basic.Constant.SERVER_URL;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.progressbar)
    ProgressBar progress;

    @BindView(R.id.products_list)
    ListView productList;

    @BindView(R.id.sideBar)
    SideBar indexBar;

    ProductAdaptor adp;

    ArrayList<Product> products;
    BelshifaDB db;

    public Integer page = 1;
    public Integer limit = 3000;
    public Boolean getMoreData = true;
    public Boolean success = true;

    public int noOfTasks = 0;

    public static final String BASE_URL = SERVER_URL;
    Gson gson;
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        products = new ArrayList<>();
        hideProgressBar();
        db = new BelshifaDB(MainActivity.this);
        checkCashedData();
    }

    public void addTask(){
        noOfTasks++;
    }

    public void removeTask(){
        noOfTasks--;
    }

    public void allTasksComplete(){

        if(noOfTasks == 0){
            if(success == true) {
                Collections.sort(products, new Comparator<Product>() {
                    @Override
                    public int compare(Product o1, Product o2) {
                        return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
                    }
                });
                fillAdaptor(products);
                saveData(products);
            }else {
                products = new ArrayList<>();
                fillDatabase();
            }
        }
        hideProgressBar();
    }

    public void saveData(ArrayList<Product> product) {
        //fill local database with incoming data
        if(products.size() > 0) {
            long result = db.insertProducts(product);
        }
    }

    public void checkCashedData() {
      //  showProgressBar();
        ArrayList<Product> productArr = db.selectProducts();
        if(productArr.size() > 0) {
            //get data from cashed
            fillAdaptor(productArr);
            hideProgressBar();
        } else {
            fillDatabase();
        }
    }

    public void fillDatabase() {
            if(page == 1) {
                showProgressBar();
            }

        while (page < 5) {

            addTask();
            MyApiEndpointInterface apiService = MyApiEndpointInterface.retrofit.create(MyApiEndpointInterface.class);
            Call<ProductData> call = apiService.getProducts(page, limit);
            call.enqueue(new Callback<ProductData>() {
                @Override
                public void onResponse(Call<ProductData> call, Response<ProductData> response) {
                    onGetProductsSuccess(((ProductData)response.body()).getData());
                }

                @Override
                public void onFailure(Call<ProductData> call, Throwable t) {
                    onGetProductsFailed();
                }
            });
            page++;
        }
    }

    public void onGetProductsSuccess(ArrayList<Product> prods) {
        removeTask();
        products.addAll(prods);
        allTasksComplete();
    }

    public void onGetProductsFailed() {
        if(page == 0) {
            hideProgressBar();
        }
        products = null;
        removeTask();
        success = false;
        allTasksComplete();
    }

    public void fillAdaptor(ArrayList<Product> data) {
        adp = new ProductAdaptor(MainActivity.this, data);
        char[] l  = setSectionArray(data);
        productList.setAdapter(adp);
        indexBar.setListView(productList, l);
    }

    public char[]  setSectionArray(ArrayList<Product> data) {
        ArrayList<String> arr = new ArrayList<>();
        Boolean flag = false;
        char[] l;
        for(int i = 0 ; i < data.size() ; i ++) {
            if(i == 0){
                if(Character.isLetter(data.get(i).getName().toUpperCase().charAt(0))) {
                    arr.add(data.get(i).getName().toUpperCase().charAt(0) + "");
                } else {
                    arr.add("#");
                    flag = true;
                }
               // l[i] =  data.get(i).getName().toUpperCase().charAt(0);
               // sectionArray.add(new Product(i, data.get(i).getName(), 0));
            } else {
                if(Character.isLetter(data.get(i).getName().toUpperCase().charAt(0))) {
                    char firstChar = data.get(i).getName().toUpperCase().charAt(0);
                    char preFirstChar = data.get(i-1).getName().toUpperCase().charAt(0);
                    if (firstChar != preFirstChar) {
                        arr.add(data.get(i).getName().toUpperCase().charAt(0) + "");
                        // l[i] =  data.get(i).getName().toUpperCase().charAt(0);
                    }
                } else {
                    if(flag == false) {
                        arr.add("#");
                        flag = true;
                    }
                }
        }

        }
        l= new char[arr.size()];
        String str=arr.toString().replaceAll(",", "");
        l = str.substring(1, str.length()-1).replaceAll(" ", "").toCharArray();
        return l;
    }


    public void showProgressBar() {
        progress.setVisibility(View.VISIBLE);
        progress.bringToFront();
    }

    public void hideProgressBar(){
        progress.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.closedDBConnection();
    }
}
