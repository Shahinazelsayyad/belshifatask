package com.example.chahinaz.belshifaproducts.database;

import android.provider.BaseColumns;

public class ProductsTables {

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ProductsTables() {}

    /* Inner class that defines the table contents */
    public static class ProductTable implements BaseColumns {
        public static final String TABLE_NAME = "product";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_PRICE = "price";
        public static final String COLUMN_NAME_ID = "id";
    }
}
