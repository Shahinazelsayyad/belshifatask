package com.example.chahinaz.belshifaproducts.entities;

import java.io.Serializable;

public class Product implements Serializable {

    private int id;
    private String name;
    private double price;

    public Product() {

    }

    public Product(int itemID, String name, double price) {
        this.id = itemID;
        this.name = name;
        this.price = price;
    }


    public int getItemID() {
        return id;
    }

    public void setItemID(int itemID) {
        this.id = itemID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
